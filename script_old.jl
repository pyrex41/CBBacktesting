import Pkg
Pkg.activate(".")
Pkg.update()
using Revise, Dates, TimeSeries

using Plots

using Backtesting
using Base.Threads

Backtesting.load_from_csv(dir="data")

res = Dict{Integer, TimeArray}()
fema = Dict{Integer, TimeArray}()
sema = Dict{Integer, TimeArray}()
trade_ind = Dict()


println("number of threads: ", nthreads())
@threads for i in candle_nums
    println("Running sim for candle size ", i)
    sc, f,s,r = emasim(candle=i, fastema=8,slowema=50, fee = 0.0025)
    res[i] = r
    fema[i] = f
    sema[i] = s
    trade_ind[i] = sc
    println("--------------------------------")
end
ii = collect(keys(res)) |> sort
foreach(ii) do i
    println(i)
    @show kelly(res[i])
    println("daily sharpe:")
    @show sharpe(res[i])
    println("----------------")
end
#=
for (i, v) in trade_ind
    nm = string(i, "_candle_trades.csv")
    ts = DateTime[]
    position = Symbol[]
    price = Float64[]
    for bs in v
        push!(ts, bs.time)
        push!(position, bs.signal |> Symbol)
        push!(price, bs.price)
    end
    ta = TimeArray(ts, hcat(position, price), [:position, :price])
    writetimearray(ta, nm)
end
=#
for (i,r) in res
    println("saving plot for ", i)
    p = plot(r[:trade_pnl, :baseline_pnl], size=(1200,800))
    savefig(p, string("fee25plot/",i, "_candle_ema_8_50_plot.png"))
end

for (i,r) in res
    println("saving logplot for ", i)
    p = plot(r[:trade_pnl, :baseline_pnl], size=(1200,800), yaxis=:log)
    savefig(p, string("fee25plot/",i, "_candle_ema_8_50_plot_log.png"))
end

fmerge(v) = foldl(v) do x,y
    m = merge(x,y, :outer)
    rename!(m, vcat(colnames(x),colnames(y)))
end
rv = [res[1][:baseline_pnl]]
ind = ["Buy_&_Hold"]
for (i,r) in res
    push!(ind, string(i,"_candle_8_50"))
    rr = r[:trade_pnl]
    rename!(rr, [Symbol(i,"_candle_8_50_pnl")])
    push!(rv,rr)
end
mm = fmerge(rv)
p = plot(mm, size=(1200,800), yaxis=:log, label=reshape(ind,1,length(ind)));
q = plot(mm, size=(1200,800), label=reshape(ind,1,length(ind)));
savefig(p, "fee25plot/all_pnl_log.png")
savefig(q, "fee25plot/all_pnl_normal.png")

writetimearray(mm, "all_pnl_fee_25.csv")
#=
m1 = fmerge(fema |> values |> collect)
m2 = fmerge(sema |> values |> collect)
mema = merge(m1,m2,:outer)
writetimearray(mema, "ema_values.csv")

function fillnan(v::Vector{Float64})
    vv = v[1:1]
    for i=2:length(v)
        a = last(vv)
        b = v[i]
        c = isnan(b) ? a : b
        push!(vv, c)
    end
    vv
end

ts = timestamp(mema)
cn = colnames(mema)
dd = []

for c in cn
    v = values(mema[c])
    vv = fillnan(v)
    push!(dd, vv)
end
rmema = TimeArray(ts, foldl(hcat, dd), cn)
writetimearray(rmema, "interpolated_ema_values.csv")
=#
