# CBBacktesting

Plots in `plots` folder

Especially check out all_pnl_log.png

csv files have data. Check out ema csvs for ema data

Added feeplot folder and csv, calculated using .075%% fees (most fees are around
.5%, added extra to be conservative and account for slippage). It makes the 1
and 5min notably less profitable based on stats but the graphs still look good.
