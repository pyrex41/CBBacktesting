using StatsPlots, DataFrames, CSV
import Base.@lock



const xrange = 0:25:1000 |> collect; xrange[1] = 1
const yrange = 50:50:1000
const nmin = 1
const title = string("faster exit - #5a - ", nmin, "min")
println(title)

println("Getting funcs")

const ta = data_dic[nmin]

const kp = []
for x in xrange
    for y in yrange
        if x < y
            push!(kp, (x,y))
        end
    end
end

const sigrange = vcat(collect(yrange), xrange) |> union |> sort
const ddict = Dict()
const fdict = Dict()
@threads for i in sigrange
    println(i)
    ddict[i] = Backtesting.ema(ta[:close], i)
    fdict[i] = Backtesting.sfunc_get(ta, na_val = Nothing)
end


const ts = timestamp(ta)
const sigdict = Dict()

@threads for i=1:length(kp)
    println(i)
    x,y = kp[i]
    fastfunc = fdict[x]
    slowfunc = fdict[y]

    sigdict[(x,y)] = CB.genaric_crossfunc(fastfunc, slowfunc)
end




#=
ysig = Dict()
@threads for y in yrange
    ysig[y], _ = CB.ma2periodcross(ta, ta, fast=y, slow=200, select=:close, thr = false, mafastfunc=Backtesting.ema, maslowfunc=Backtesting.ema)
end
=#

const fulock = ReentrantLock()
#=
@threads for i=1:length(kp)
    x,y = kp[i]
    let
        x,y = kp[i]
        println("Getting func for x: ", x, ", y: ", y)
        sig = xsig[x,y]
        fu = CB.ma_aggr_exit_helper(sig, sig, timestamp(ta))
        @lock fulock funci[(x,y)] = fu
    end
end

=#


const te = Dict()
const p_count = zeros(length(kp))
@time @thread for i=1:10#length(kp)
    let
        tup = kp[i]
        func = sigdict[tup]

        p_count[i] = 1
        p = sum(p_count)

        local res = CB.runfunccontained(func, fee=0.0, global_store=false, ii=(p, length(kp)), thr=false)
        @lock fulock te[tup] = res
    end
end

#=

println("calculating Kelly for all paths")
df = DataFrame(X=Int[], Y=Int[], Z=Float64[])
dflock = ReentrantLock()
@threads for i=1:length(kp)
    x,y = kp[i]
    z = kelly(te[kp[i]])[2].second
    @lock dflock push!(df, (x,y,z))
end
println("writing to CSV")
CSV.write(string(title,".csv"), df)

println("Plotting........")
p = @df df surface(:X,:Y,:Z, size=(1200,800), xlabel="Fast EMA", ylabel="SLOW EMA", zlabel="Kelly", title=title)

png(p, title)
#=

te_fees = Dict()
p_count = zeros(length(funck))
@threads for i=1:length(funck)
    tup = funck[i]
    func = funci[tup]

    p_count[i] = 1
    p = sum(p_count)

    local res = CB.runfunccontained(func, fee=0.05, global_store=false, ii=(p, length(funck)), thr=false)
    te_fees[tup] = res
end

println("calculating Kelly for all paths -- fees")
df_fees = DataFrame(X=Int[], Y=Int[], Z=Float64[])
kk = collect(keys(te))
@threads for i=1:length(kk)
    x,y = kk[i]
    z = kelly(te_fees[kk[i]])[2].second
    @lock dflock push!(df_fees, (x,y,z))
end
println("writing to CSV")
title = string(title, " - Fee = 0.5%")
CSV.write(string(title,".csv"), df)

println("Plotting........")
p = @df df surface(:X,:Y,:Z, size=(1200,800), xlabel="Fast EMA", ylabel="Slow EMA", zlabel="Kelly", title=title)

png(p, title)
=#

println("done")
=#
