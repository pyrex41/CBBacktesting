import Pkg
Pkg.activate(".")
Pkg.update()
using Revise, Dates, TimeSeries

using Plots

using Backtesting, CB, ML
using Base.Threads

#Backtesting.loadfromcsv(dir="data", update=false)
#Backtesting.loadfromcsv(dir="data", update=false)
od = pwd()
ddir = "/Users/reuben/CBBacktesting/data/"

update = false

if length(ARGS) == 0 || ARGS[1] != "false"
    cd(string(ddir, "BTC-USD"))
    data_btcusd = Backtesting.loadfromdir(update=update)
    const ddd = Dict((BTC,USD) => data_btcusd)
    println("BTC-USD loaded")

    cd(string(ddir, "ETH-USD"))
    data_ethusd = Backtesting.loadfromdir(update=update, pair=(ETH,USD))
    ddd[(ETH,USD)] = data_ethusd
    println("ETH-USD loaded")

    cd(string(ddir, "ETH-BTC"))
    data_ethbtc = Backtesting.loadfromdir(update=update, pair=(ETH,BTC))
    ddd[(ETH,BTC)] = data_ethbtc
    println("ETH-BTC loaded")
end

cd(od)
